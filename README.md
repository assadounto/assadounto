<div id="header" align="center">
  <img src="https://media.giphy.com/media/M9gbBd9nbDrOTu1Mqx/giphy.gif" width="100"/>
  <div id="badges">
    
  <a href="https://www.linkedin.com/in/richmond-adu-kyere-911693236">
    <img src="https://img.shields.io/badge/LinkedIn-blue?style=for-the-badge&logo=linkedin&logoColor=white" alt="LinkedIn Badge"/>
  </a>
  <a href="https://api.whatsapp.com/send?phone=233556255233&text=Hi%20there!%20Thanks%20for%20reaching%20out!%20I%20will%20be%20with%20you%20in%20a%20moment!">
    <img src="https://img.shields.io/badge/WhatsApp-25D366?style=for-the-badge&logo=whatsapp&logoColor=white"/>
  </a>
  <a href="your-twitter-URL">
    <img src="https://img.shields.io/badge/Twitter-blue?style=for-the-badge&logo=twitter&logoColor=white" alt="Twitter Badge"/>
     
  </a>
</div>
  <img src="https://komarev.com/ghpvc/?username=assadountoflat-square&color=blue" alt=""/>
  <div align="center">
</div>


</div>

---

### 👨‍🎓: About Me :
  I am an Electrical Engineer and Full Stack Developer <img src="https://media.giphy.com/media/WUlplcMpOCEmTGBtBW/giphy.gif" width="30"> from Ghana.
- :telescope: I’m contributing to frontend and backend for building web applications.

- :seedling: Exploring Artificial Intelligence.

- :zap: In my free time, I solve problems on hackerrank, read tech articles and code Arduino and Rasberrypi

- :mailbox:How to reach me: [![Linkedin Badge](https://img.shields.io/badge/-richadu-blue?style=flat&logo=Linkedin&logoColor=white)](www.linkedin.com/in/richmond-adu-kyere-911693236)<a href="mailto:adukyerer@gmail.com"> <img src="https://img.shields.io/badge/richadu-D14836?style=flat&for-the-badge&logo=gmail&logoColor=white" alt="gmail"/></a>


---

### :hammer_and_wrench: Languages and Tools :
<div>
Html Css JavaScript  Git Teact Ruby Rails MySQL SQL herokuapp Netlify Python C++
</div>

---

### :bulb: Prototypes :
 <div id="badges">
    
  <a href="#">
    <img src="https://img.shields.io/badge/Arduino-00979D?style=for-the-badge&logo=Arduino&logoColor=white" alt="LinkedIn Badge"/>
  </a>
  <a href="#">
    <img src="https://img.shields.io/badge/Raspberry%20Pi-A22846?style=for-the-badge&logo=Raspberry%20Pi&logoColor=white"/>
  </a>
  
  </div>
  
---

### :fire: My Stats :
[![GitHub Streak](http://github-readme-streak-stats.herokuapp.com?user=assadounto&theme=dark&background=000000)](https://git.io/streak-stats)

<div> <!-- Markdown -->
  <h2 class"pt-2">Thanks for visiting my profile here is a joke for you</h2>
<div><img src="https://readme-jokes.vercel.app/api" alt="Jokes Card" /></di></di>
<!---
assadounto/assadounto is a ✨ spe
cial ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
